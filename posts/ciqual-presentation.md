title: Guile pour manipuler la table de composition Ciqual
date: 2020-01-13 23:00:00
tags: gnu guile scheme pratique tdd ciqual nutrition
---

# Présentation

Je me lance dans le développement d'un outil (nom à définir) qui me permettra de manipuler la table de composition du site [Ciqual de l'ANSES](https://ciqual.anses.fr/).

Dans un premier temps, disons une version 0.1, l'outil sera disponible en ligne de commande pour les systèmes GNU/Linux. J'aimerais que pour un aliment donné (via son nom ou son code dans la table), l'outil me retourne la composition nutritionnelle de cette aliment.

Exemple :

```
(composition 1000)
⇒ ((ALIM
     (alim_code " 1000 ")
     (alim_nom_fr " Pastis ")
     (COMPO
      (CONST
       (const_code " 327 ")
       (const_nom_fr " Energie, Règlement UE N° 1169/2011 (kJ/100g) ")
       (teneur " 1140 ")))))
```

## Les données de Ciqual

Ciqual est une table de composition nutritionnelle française des aliments. D'après le site, elle couvre : 2807 aliments et 61 constituants. Je ne connais pas bien l'écosytème des ressources en nutrition mais j'imagine que c'est assez complet.

L'interface de cette table est un moteur de recherche en ligne qui dispense la composition nutritionnelle d'un l'aliment sélectionné.

Il est aussi possible de récupérer les données sous différentes formats, notamment en xml, afin de les utiliser hors ligne.

La table au format xml est répartie en cinq fichiers :
- alim_2017 11 21.xml
- compo_2017 11 21.xml
- sources_2017 11 21.xml
- alim_grp_2017 11 21.xml
- const_2017 11 21.xml

Chacun de ces fichiers contient des informations que je souhaite croiser pour dispenser l'information la plus complète possible.

# Apprentissage

Cette première version de l'outil va être pour moi l'occasion d'apprendre à manipules des données au format xml avec le langage Guile.

Bien que le Guile dispose de son interpreteur, je préfère toujours la bonne vielle méthode du TDD pour apprendre et développer en même temps.
