title: Les carnets d'un Guile Hacker
date: 2020-02-14 18:00
tags: guile blog carnet hacker
---

Un carnet centralisent des notes à propos d'un sujet spécifique. Là où un billet
de blog apporte une information momentanée et parfois éparse, le carnet
se veut, lui, intemporel et plus complet.

Dans mes carnets, je vous présente les outils que j'utilise pour coder en
Guile, les méthodes que j'applique, ... Le but étant d'aller un peu plus
loin qu'avec mes billets de blog et de garder ces carnets le plus possible à jour.

Voilà, ça se passe par [ici](carnets.html) !

