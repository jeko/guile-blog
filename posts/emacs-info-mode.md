title: Emacs Info Mode
date: 2020-02-10 21:35
tags: emacs mode info
---

Les logiciels du projet [GNU](https://www.gnu.org/) diposent d'une
documentation bien fournie aux formats suivants : html, info, pdf, pour ne
citer que ceux là.

Pour pouvoir profiter de cette documentation sans quitter
[Emacs](https://www.gnu.org/software/emacs/), il existe un mode qui
exploite la documentation formatée en *.info* et permet de s'y déplacer,
faire des recherches, ...

Ça devient vraiment pratique quand, par exemple, je me demande quels arguments
une fonction a besoin et que l'éditeur est capable d'ouvrir la
documentation de cette fonction !

